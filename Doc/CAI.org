* Overview:
  - Custom AI (CAI) is an in-game GUI system that help player creates and modify behaviors (AI) of the game characters.
  - Usage:
    - CAI can be useful in team based game where player can focus controling 1 guy, and let the the custom AI drive other party member.
    - CAI can be used in RTS to reduce micro management

* Research:
  ** Baldur's Gate II:
    - Player can choose character’s behavior from a list of AI scripts. Beside pre-built AI scripts that the game provides, player can create his own scripts using Baldur’s Gate’s scripting language and compile it using AICompile.exe that included in game.
    - Player can not modify the AI in game, can only choose from a preset
    - E.g: 
      IF
        See([ENEMY]) 
      THEN
        RESPONSE #100
          RunAwayFrom([ENEMY])
      END
    - Ref: [[www.citilink.com/~jch/bg/]]
  ** Final Fantasy XII:
    - Gambit system: GUI editor allow player to change the party members' AI
    - Each gambit has two parts: a target with its condition and an action. When the condition of the target happens, the character will execute the related action on that target. When there are multiple gambits can be executed, the system chooses the gambit has highest priority.
    - Pros:
      - First in-game GUI editor that allow players to change AI in real time
      - When in combat, the game show lines indicate what actions and targets that a characters trying to do, it help to figure out why the AI working/not working.
    - Cons: 
      - Each gambit contains only one target and one related action. If player want to apply same action to different targets, he must create gambit for each of them. Player must do them same thing when he want to apply more than one action (series of actions) to one target.
      - The Gambit system also does not allow player to specify the number of actions like “drink two health-pots”. There are no ways to specify the number of time the character need to execute an action.
      - For some reasons, the Gambit system only checks the condition of the target but not the condition of the action. When the condition of a target is satisfied even if the character cannot execute the action (run out of items, mana); he still try to do that and end up with a fail action. This issue breaks the logical of behavior programming, player must check manually whether character can execute an action or not.
  ** Kodu:
    - Kodu provides a visual interface that supports behavior programming. It uses icons and subtext to represent game objects.
    - The grammar of Kodu consists of two components: a "condition" and an "action". When the condition happens, the character executes the corresponding action.
      - A condition created from sensors and filters.
        - Sensors are the interaction between character with other game objects or the input from player.
        - Filters specify game objects that character interacts.
      - An action is combination of a verb and modifiers.
        - A verb is a behavior of the character.
        - Modifiers specify how the action needs to be done.
      - E.g: “See red fruit move forward quickly”
        - This behavior means whenever character see a red fruit, it will move toward that fruit quickly.
        - “See red fruit” is the condition, which has ‘see’ as the sensor and ‘red’, ‘ fruit’ are filters.
        - “Move forward quickly” is the corresponding action, which has “move” as the verb and ‘forward’, ‘quickly’ are modifiers
    - Pros:
      - The grammar of Kodu makes the block of code look like normal English sentences, which is easy for player to understand without any programming knowledge.
      - Kodu also allow player to change a group of character’s behavior – a page with keyword switch. Player can create different pages for different states of character, and change the state when a condition happens. This feature fixes the problem of Gambit system that need to be changed usually; but if it is overused, it can make the system complicated.
      - Kodu also has an in-game debugging system; which shows what character is thinking, what it intends to do and where it intends to move to.
    - Cons:
      - It'd be better if the game provides different interface for different programming levels like: beginner and advanced. In advanced interface, player can create his own condition with operators that are more complex.
      - Another issue is the use of page, it can confuse player when it is overused. One solution for this problem is not allowing player to change to another page but allowing enable or disable a page.
  ** Dragon Age - Origin:
    - The game provides Tactics system which allow player to create and modify the AI scheme of characters. This Tactics system is similar to Gambit system in Final Fantasy XII in many ways but it also adds some good tweak to it.
    - Tactics system is a rule-based AI system. The grammar for a rule contains 2 parts: [Target]:[Condition] and [Action].
      - The first part specifies what type of target and when the character should use the action on that target. For each type of target, the character will check all characters of that target type that it can see and check whether that character satisfies the condition.
      - If the character find a target have condition as described, it will do the action to that target.
    - Pros:
      - For each character, player can create more than one group of AI rules (tactic scheme) and set the name for that scheme. Player can change a group of AI rules to others by changing the scheme of the character, no need to re-modify the same scheme. This helps reducing the time to change the AI in different environment.
      - Another improvement of Tactics system over Gambit system is skills’ icons. Instead of using text for the name of skills Tactics system using the icons for the skills. Those icons are the same as skills’ icons in the game which help player to recognize them faster and take less time to choose the right skill (don’t need to read about that skill’s info as player already familiar with it in the game).
    - Cons:
      - Player can only specify 1 condition for a target which makes it hard to choose the right condition for the target player want to apply the action for. To cover this, the game tries to provide a lot of different and detail conditions but this leads to other issue. 
      - The game use text to describe the target and condition which make choosing the right condition from a long list of text may be tedious and error-prone.
      - Another problem is the limited number of tactic slots. Player starts the game with 6 slots and only get new slot when the character get to a specified level. This limit make the Tactics system less useful cause the number of spell a character have always larger than the number of Tactics slot.
  ** Conclusion:
    - Most of the custom AI system use rule-based AI system with a light state machines (the use of page/state and allow to switch state)
    - It's better to use icons instead of text
    - Kodu's English description of the rule are good, it help players understand the system and learn it faster
    - Debug information (e.g: Kodu, FF XII) is a must
    - Having AI script system (e.g: BG2) allow players to add and exchange a lot more custom AI to the game, reduce the work of the dev

* Design:
  - Rule based AI system with state support
  ** Rule:
    - Each rule contains 3 components:
      - Target: the type of target that character will apply the Action on
      - Conditions: list of all the conditions that the target need to satisfy in order to trigger the rules
      - Action: the action the character will do when the target satisfy the condition
    - Rules are checked top down on the list, when a rules are satisfy then it will be triggered
    - A rule is satisfy when:
      - There is 1 or more target in the character's perception range (vision or hear)
      - That target satisfy all the conditions
      - Player can do the action (e.g: have enough mana, can move ...) on that character (e.g: target in range of attack)
  ** State:
    - Each AI state contain multiple rules
    - A state can be saved, loaded to a AI description file and named by player
    - Player can turn on/off a state
    - There is action to switch state
  ** AI description file:
    State: StateName
    [
      Rule: [On,Off]
      Target: [TargetType]
      Action: ActionName
      Condition
      {
        [ConditionType]:[ConditionValue]
      }
    ]*
  ** AI processor:
    - Each frame, the AI parser process character’s AI state and choose the action to do. It checks all the rules in an AI state to find the action need to be executed. For a rule, if its conditions are satisfied, its action will be executed. The engine checks the rules depend on their priority. The rules with higher priority will be checked before the others. When a rule is triggered, the engine stops checking; and ignore the rules with lower priority. 

    PotentialTarget = CharactersInSight(TargetType)
    // Remove all characters don't satisfy conditions
    Foreach Condition in ConditionList
        Foreach Character in PotentialTarget
            if Character not have Condition
                Remove Character from PotentialTarget
        End
    End
    // Remove all characters that the action can't apply on
    Foreach Character in PotentialTarget
        if Action not executable to Character
            Remove Character from PotentialTarget
        End
    End
    if PotentialTarget is Empty
        ProcessNextRule()
    else
        Character = PotentialTarget[0]
        Execute Action on Character
    End
  ** Debug info:
    - Player can toggle the AI debug info
    - Show action that a character trying to do over his head
    - Draw a line from a character to its target
    - Advance: have a test room which spawn the target that satisfy the conditions and see the rule happen.
  ** Description text:
    - When player change the rule, there is plain English text describe what's the rule about. e.g: Cast Healing spell on yourself when your health down below 30%
    - Allow player's comment?
  ** UI:
    - Use icon for actions, target and conditions when building the rule in the CAI editor.
    - Use different color and shape for diferent type of target, action icons.
      - Color:
        - Red for enemies target, attack/harm action
        - Green for friendly target, buff action
        - Blue for self, healing/revive action
        - Black for neutral target, debuff action
      - Shape:
        - Square for actions
        - Circle for conditions
        - Triangle for rule index
        - Round-corner square for targets.

* Targets:
  - NPC – all character types
  - Foe – enemies, hostile characters
  - Ally – other team members
  - Self – the character which own this AI state
  - Player – player-controlled character
  - Team – all team members
  - "Named" - a specific type of character. E.g: Goblin, Soldier
    - Imply this is enemies?
    - Can combo with other target type?

* Conditions:
  - Effect condition: check the effects that are applying on the target
    - This is on-going FX like buff, debuff
  - Elemental condition: check target’s elemental type
    - We can use a general tag system. E.g: a monster can have multiple tags: "Fly", "Fire" 
  - Distance condition: check the distance between owner of the rule and the target. E.g: close, far away ...
  - Status condition: check target’s HP/MP/Stamina status. E.g: HP < 20%, dead ...

* Action:
  ** Basic actions:
    - Movement: 
      - Move to: a target location. The target location can be a dynamic, moving target.
      - Move away: from a target - try to keep distance from a target
    - Use item: use an item in the character's inventory
      - Can be swap weapon sets
    - Trigger a skill: trigger a skill in the character's skill list
      - Can sort skill or categorize skills so we can reuse the rule on multiple characters. E.g: Use the highest DPS skill on the boss enemies.
  ** Squential actions:
    - Combine of multiple actions
    - Can other rules interupt this?