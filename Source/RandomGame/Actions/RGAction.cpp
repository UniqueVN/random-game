// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "RGAction.h"


URGAction::URGAction(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{

}

void URGAction::Start(APawn* PawnTarget)
{
	SetOwner(PawnTarget);
	OnStarted();
}

void URGAction::Stop()
{
}

APawn* URGAction::GetOwner() const
{
	return PawnOwner;
}

void URGAction::SetOwner(APawn* NewOwner)
{
	PawnOwner = NewOwner;
}

void URGAction::OnStarted()
{
	K2_ActionStarted(PawnOwner);
}

