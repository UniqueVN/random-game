// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "RGAction.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API URGAction : public UObject
{
	GENERATED_BODY()

public:
    URGAction(const class FObjectInitializer& ObjectInitializer);

	//UFUNCTION(BlueprintCallable, Category = "Ability", FriendlyName = "Activate")
	void Start(APawn* PawnTarget);

	void Stop();

	UFUNCTION(BlueprintCallable, Category = "Ability", FriendlyName = "GetOwner")
	APawn* GetOwner() const;

	
protected:
	/** Pawn that owns this ability. */
	UPROPERTY()
	APawn* PawnOwner;

	virtual void OnStarted();

	UFUNCTION(BlueprintImplementableEvent, Category = Ability, FriendlyName = "OnActionStartedEvent")
	virtual void K2_ActionStarted(class APawn* PawnTarget);

private:
	void SetOwner(APawn* NewOwner);
};
