// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "RGSteeringComponent.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API URGSteeringComponent : public UActorComponent
{
	GENERATED_BODY()

public:
    URGSteeringComponent(const class FObjectInitializer& ObjectInitializer);

	void MoveTo(AActor* Target);

	FVector GetDesiredDirection();

private:
	
	UPROPERTY(transient)
	AActor* TargetActor;
};
