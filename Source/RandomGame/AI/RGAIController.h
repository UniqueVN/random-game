// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "RGAIController.generated.h"

class URGSteeringComponent;

DECLARE_DELEGATE_ThreeParams(FMakeDecisionDelegate, class ARGAIController*, class ARGCharacter*, float);

/**
 * 
 */
UCLASS()
class RANDOMGAME_API ARGAIController : public AAIController
{
	GENERATED_BODY()

public:
    ARGAIController(const class FObjectInitializer& ObjectInitializer);

	URGSteeringComponent* GetSteeringComponent();

	void MakeDecision(float MoveDuration);
	void SetDecisionMaker(FMakeDecisionDelegate const& Delegate);

private:

	FMakeDecisionDelegate MakeDecisionDelegate;
	
	UPROPERTY()
	URGSteeringComponent* SteeringComponent;
};
