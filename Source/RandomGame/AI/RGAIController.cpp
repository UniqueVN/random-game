// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "RGAIController.h"
#include "RGSteeringComponent.h"
#include "RGCharacter.h"

ARGAIController::ARGAIController(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    SteeringComponent = ObjectInitializer.CreateDefaultSubobject<URGSteeringComponent>(this, TEXT("SteeringComponent"));
}

void ARGAIController::MakeDecision(float MoveDuration)
{
	if (GetPawn() != NULL)
	{
		ARGCharacter* character = CastChecked<ARGCharacter>(GetPawn());
		MakeDecisionDelegate.ExecuteIfBound(this, character, MoveDuration);
	}
}

void ARGAIController::SetDecisionMaker(FMakeDecisionDelegate const& Delegate)
{
	MakeDecisionDelegate = Delegate;
}

URGSteeringComponent* ARGAIController::GetSteeringComponent()
{
	return SteeringComponent;
}