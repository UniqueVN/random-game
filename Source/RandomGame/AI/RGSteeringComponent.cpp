// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "RGSteeringComponent.h"
#include "RGAIController.h"

URGSteeringComponent::URGSteeringComponent(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
}

void URGSteeringComponent::MoveTo(AActor* Target)
{
	verify(Target != NULL);
	TargetActor = Target;
}

FVector URGSteeringComponent::GetDesiredDirection()
{
	ARGAIController* ai = CastChecked<ARGAIController>(GetOwner());
	APawn* self = ai->GetPawn();
	if (self != NULL && TargetActor != NULL)
	{
		return (TargetActor->GetActorLocation() - self->GetActorLocation());
	}
	
	return FVector::ZeroVector;
}

