// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "BTTask_DecisionBase.h"
#include "AI/RGAIController.h"

UBTTask_DecisionBase::UBTTask_DecisionBase(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{

}

EBTNodeResult::Type UBTTask_DecisionBase::ExecuteTask(UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type result = Super::ExecuteTask(OwnerComp, NodeMemory);
		
	if (result == EBTNodeResult::InProgress || !bImplementsReceiveAbort)
	{
		ARGAIController* ai = Cast<ARGAIController>(OwnerComp->GetOwner());
		ai->SetDecisionMaker(FMakeDecisionDelegate::CreateUObject(this, &UBTTask_DecisionBase::MakeDecision));

		CurrentCallResult = EBTNodeResult::InProgress;
		return CurrentCallResult;
	}

	return result;
}

void UBTTask_DecisionBase::MakeDecision(class ARGAIController* AI, class ARGCharacter* Character, float MoveDuration)
{

}

