// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIModule.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BehaviorTree/Tasks/BTTask_BlueprintBase.h"
#include "BTTask_DecisionBase.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API UBTTask_DecisionBase : public UBTTask_BlueprintBase
{
	GENERATED_BODY()

public:
    UBTTask_DecisionBase(const class FObjectInitializer& ObjectInitializer);

	virtual EBTNodeResult::Type ExecuteTask(class UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory) override;

protected:
	UFUNCTION(BlueprintImplementableEvent)
	virtual void ReceiveMakeDecision(AActor* OwnerActor);

private:
	void MakeDecision(class ARGAIController* AI, class ARGCharacter* Character, float MoveDuration);
};
