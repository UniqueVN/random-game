// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "BTTask_Engage.h"
#include "AI/RGAIController.h"
#include "AI/RGSteeringComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "RGCharacter.h"

UBTTask_Engage::UBTTask_Engage(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
	NodeName = "Engage Target";

	// accept only actors
	BlackboardKey.AddObjectFilter(this, AActor::StaticClass());
}

EBTNodeResult::Type UBTTask_Engage::ExecuteTask(UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory)
{
	const UBlackboardComponent* blackboard = OwnerComp->GetBlackboardComponent();
	if (blackboard == NULL)
	{
		return EBTNodeResult::Failed;
	}
	
	verify(BlackboardKey.SelectedKeyType == UBlackboardKeyType_Object::StaticClass());
	AActor* target = Cast<AActor>(blackboard->GetValueAsObject(BlackboardKey.GetSelectedKeyID()));
	if (target == NULL)
	{
		return EBTNodeResult::Failed;
	}

	ARGAIController* ai = CastChecked<ARGAIController>(OwnerComp->GetOwner());
	ai->GetSteeringComponent()->MoveTo(target);
	ai->SetDecisionMaker(FMakeDecisionDelegate::CreateUObject(this, &UBTTask_Engage::MakeDecision));

	return EBTNodeResult::InProgress;
}

void UBTTask_Engage::MakeDecision(class ARGAIController* AI, class ARGCharacter* Character, float MoveDuration)
{
	FVector direction = AI->GetSteeringComponent()->GetDesiredDirection();
	direction.Z = 0.f;
	if (direction.Normalize())
	{
		Character->K2_PickMovement(direction);
	}
}
