// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIModule.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_Engage.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API UBTTask_Engage : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:
    UBTTask_Engage(const class FObjectInitializer& ObjectInitializer);

	virtual EBTNodeResult::Type ExecuteTask(class UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory) override;

private:
	void MakeDecision(class ARGAIController* AI, class ARGCharacter* Character, float MoveDuration);
};
