// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/DamageType.h"
#include "IceDamage.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API UIceDamage : public UDamageType
{
	GENERATED_BODY()

public:
    UIceDamage(const class FObjectInitializer& ObjectInitializer);
	
};
