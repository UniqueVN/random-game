// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/DamageType.h"
#include "StoneDamage.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API UStoneDamage : public UDamageType
{
	GENERATED_BODY()

public:
    UStoneDamage(const class FObjectInitializer& ObjectInitializer);
	
};
