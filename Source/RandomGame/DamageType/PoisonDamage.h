// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/DamageType.h"
#include "PoisonDamage.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API UPoisonDamage : public UDamageType
{
	GENERATED_BODY()

public:
    UPoisonDamage(const class FObjectInitializer& ObjectInitializer);
	
};
