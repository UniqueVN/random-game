// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/DamageType.h"
#include "ElectricDamage.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API UElectricDamage : public UDamageType
{
	GENERATED_BODY()

public:	
    UElectricDamage(const class FObjectInitializer& ObjectInitializer);
};
