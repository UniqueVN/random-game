// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/DamageType.h"
#include "FireDamage.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API UFireDamage : public UDamageType
{
	GENERATED_BODY()

public:	
    UFireDamage(const class FObjectInitializer& ObjectInitializer);
};
