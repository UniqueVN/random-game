// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "RandomGameGameMode.h"
#include "RGAbility.h"
#include "LatentActions.h"
#include "RGCharacter.h"
#include "RGMove.h"


ARGCharacter::ARGCharacter(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer),
	PickedMove(NULL)
{
    Health = 1000.f;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement    
    GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
    GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
    GetCharacterMovement()->JumpZVelocity = 600.f;
    GetCharacterMovement()->AirControl = 0.2f;
}

void ARGCharacter::BeginPlay()
{
    Super::BeginPlay();

    // Populate the Abilities list
    for (auto AbilityClass : AbilityClasses)
    {
        AddAbility(AbilityClass);
    }
}

void ARGCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (PickedMove && PickedMove->IsRunning())
	{
		PickedMove->Tick(DeltaSeconds);
	}
}

URGAbility* ARGCharacter::AddAbility(TSubclassOf<class URGAbility> NewAbilityClass)
{
    // TODO: Give error when compiling blueprint if NewAbilityClass is NULL
    if (!NewAbilityClass)
        return NULL;

    URGAbility* NewAbility = (URGAbility*)StaticConstructObject(NewAbilityClass);

    if (!NewAbility)
        return NULL;

    Abilities.Add(NewAbility);
    NewAbility->SetOwner(this);

    return NewAbility;
}

void ARGCharacter::ActivateAbility(URGAbility* Ability, APawn* PawnTarget)
{
    // TODO: Give error when compiling blueprint if NewAbility is NULL
    if (!Ability)
        return;

    Ability->Activate(PawnTarget);
}

void ARGCharacter::ActivateAbilityAtIndex(int32 Index, APawn* PawnTarget, FLatentActionInfo LatentInfo)
{
    if (Index < 0 || Index > Abilities.Num())
        return;

    FLatentActionManager& LatentActionManager = GetWorld()->GetLatentActionManager();
    FPendingLatentAction* Action = LatentActionManager.FindExistingAction<FPendingLatentAction>(LatentInfo.CallbackTarget, LatentInfo.UUID);

    if (!Action)
    {
        LatentActionManager.AddNewAction(LatentInfo.CallbackTarget, LatentInfo.UUID, new FPendingLatentAction());
    }

    ActivateAbility(Abilities[Index], PawnTarget);
}

URGMove* ARGCharacter::PickMove(TSubclassOf<URGMove> moveType)
{
	check(PickedMove == NULL);
	PickedMove = NULL;

	for (auto moveInfo : RegisteredMoves)
	{
		if (moveInfo.MoveType == moveType)
		{
			PickedMove = moveInfo.MoveInstance;
			break;
		}
	}

	if (PickedMove == NULL)
	{
		FRegisteredMove newMoveInfo;
		newMoveInfo.MoveType = moveType;
		newMoveInfo.MoveInstance = PickedMove = ConstructObject<URGMove>(moveType, this);
		RegisteredMoves.Add(newMoveInfo);
	}

	PickedMove->Init(this);

	return PickedMove;
}

bool ARGCharacter::MakeMove(float Duration)
{
	if (PickedMove != NULL)
	{
		PickedMove->Execute(Duration);
		return true;
	}

	return false;
}

void ARGCharacter::OnMoveFinished()
{
	check(PickedMove != NULL);
	PickedMove->Reset();
	PickedMove = NULL;

	ARandomGameGameMode* game = CastChecked<ARandomGameGameMode>(GetWorld()->GetAuthGameMode());
	check(game != NULL);
	game->OnCharacterMoveFinished(this);
}


void ARGCharacter::ReceiveAnyDamage(float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
    Health -= Damage;

    // Call the super function to trigger the blueprint event
    Super::ReceiveAnyDamage(Damage, DamageType, InstigatedBy, DamageCauser);

    if (Health < 0)
        OnDead();
}

void ARGCharacter::Heal(float HealAmount)
{
    if (IsDead())
        return;

    Health += HealAmount;
}

bool ARGCharacter::IsDead()
{
    return Health <= 0;
}

void ARGCharacter::OnDead_Implementation()
{
    // FIXME: This not work for some reason, need to change the function flag so we can call the SpawnPickup event from here 
    // and also able to implement the event in the blueprint
    SpawnPickup();
}

void ARGCharacter::SpawnPickup_Implementation()
{

}


//////////////////////////////////////////////////////////////////////////
// Input

void ARGCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// Set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	InputComponent->BindAxis("MoveForward", this, &ARGCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ARGCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &ARGCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &ARGCharacter::LookUpAtRate);

	// handle touch devices
	InputComponent->BindTouch(IE_Pressed, this, &ARGCharacter::TouchStarted);
	InputComponent->BindTouch(IE_Released, this, &ARGCharacter::TouchStopped);
}


void ARGCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void ARGCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void ARGCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ARGCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ARGCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		//AddMovementInput(Direction, Value);
		HandleMoveInput(Direction * Value);
	}
}

void ARGCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		//AddMovementInput(Direction, Value);
		HandleMoveInput(Direction * Value);
	}
}

void ARGCharacter::HandleMoveInput(const FVector& Direction)
{
	ARandomGameGameMode* game = CastChecked<ARandomGameGameMode>(GetWorld()->GetAuthGameMode());
	if (game->GetTurnState() == GameTurnStateWaiting)
	{
		if (K2_PickMovement(Direction) != NULL)
		{
			game->OnPlayerMovePicked();
		}
	}
}
