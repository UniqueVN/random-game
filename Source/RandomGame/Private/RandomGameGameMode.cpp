// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "RandomGame.h"
#include "RandomGameGameMode.h"
#include "RGCharacter.h"
#include "AI/RGAIController.h"

ARandomGameGameMode::ARandomGameGameMode(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer),
	MoveDuration(0.5f),
	TurnState(GameTurnStateWaiting),
	NumPendingMoves(0)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Characters/MyCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ARandomGameGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (GetMatchState() == MatchState::InProgress)
	{
		if (TurnState == GameTurnStateProcessing)
		{
			if (NumPendingMoves <= 0)
			{
				check(NumPendingMoves == 0);
				StartWaitingTurn();
			}
		}
	}
}

void ARandomGameGameMode::OnPlayerMovePicked()
{
	check(TurnState == GameTurnStateWaiting);

	for (FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator)
	{
		ARGAIController* ai = Cast<ARGAIController>(*Iterator);
		if (ai != NULL)
		{
			ai->MakeDecision(MoveDuration);
		}
	}

	StartProcessingTurn();
}

void ARandomGameGameMode::OnCharacterMoveFinished(class ARGCharacter* Char)
{
	check(TurnState == GameTurnStateProcessing);
	NumPendingMoves--;
}

void ARandomGameGameMode::StartWaitingTurn()
{
	TurnState = GameTurnStateWaiting;
}

void ARandomGameGameMode::StartProcessingTurn()
{
	TurnState = GameTurnStateProcessing;

	check(NumPendingMoves == 0);
	NumPendingMoves = 0;

	for (FConstControllerIterator Iterator = GetWorld()->GetControllerIterator(); Iterator; ++Iterator)
	{
		AController* controller = *Iterator;
		ARGCharacter* character = Cast<ARGCharacter>(controller->GetPawn());

		if (character != NULL)
		{
			if (character->MakeMove(MoveDuration))
			{
				NumPendingMoves++;
			}
		}
	}
}
