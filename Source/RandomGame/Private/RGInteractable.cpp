// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "RGInteractable.h"


ARGInteractable::ARGInteractable(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    CollisionComp = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("CollisionComp"));
    RootComponent = CollisionComp;
    CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &ARGInteractable::OnCollisionCompTouch);

    Mesh = ObjectInitializer.CreateOptionalDefaultSubobject<UStaticMeshComponent>(this, TEXT("Mesh"));
    if (Mesh)
    {
        Mesh->AlwaysLoadOnClient = true;
        Mesh->AlwaysLoadOnServer = false;
        Mesh->bOwnerNoSee = false;
        Mesh->bCastDynamicShadow = true;
        Mesh->bAffectDynamicIndirectLighting = true;
        Mesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;
        Mesh->SetCollisionProfileName(TEXT("CharacterMesh"));
        Mesh->bGenerateOverlapEvents = false;
        Mesh->bCanEverAffectNavigation = false;
        Mesh->AttachParent = CollisionComp;
    }
}


void ARGInteractable::Activate(AActor* Activator)
{
    OnActivated(Activator);
}

bool ARGInteractable::ShouldHandleActorTouch(AActor* Other) const
{
    // If there is no triggerable classes specified then we handle all actor
    if (TriggerableActorClasses.Num() == 0)
        return true;

    for (auto TriggerableClasses : TriggerableActorClasses)
    {
        if (Other->GetClass()->IsChildOf(TriggerableClasses))
            return true;
    }

    return false;
}

void ARGInteractable::OnCollisionCompTouch(AActor* Other, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
    if (!ShouldHandleActorTouch(Other))
        return;

    OnTouch(Other);

    if (bActivateInTouch)
        Activate(Other);
}