// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "RGMove.h"


URGMove::URGMove(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{

}

void URGMove::Init(ARGCharacter* Char)
{
	check(Char != NULL);
	Owner = Char;
}

void URGMove::Reset()
{
	// TODO: clear all properties

	// TODO: clear all latent actions
}

void URGMove::Execute(float Duration)
{
	Running = true;
	K2_OnExecuted(Owner, Duration);
}

void URGMove::Finish()
{
	Running = false;
	check(Owner != NULL);
	Owner->OnMoveFinished();
}

void URGMove::Tick(float DeltaSeconds)
{
	K2_OnTicked(DeltaSeconds);
}
