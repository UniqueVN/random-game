// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "RGAbility.h"


URGAbility::URGAbility(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{

}

bool URGAbility::CanBeTrigger(APawn* Target) const
{
    return PawnOwner != NULL && !bIsActivated;
}

void URGAbility::Activate(APawn* NewPawnTarget)
{
    if (!CanBeTrigger(NewPawnTarget))
        return;

    bIsActivated = true;
    PawnTarget = NewPawnTarget;

    K2_OnActivatedEvent(PawnTarget);
}

void URGAbility::Deactivate()
{
    K2_OnDeactivatedEvent();

    PawnTarget = NULL;
    bIsActivated = false;
}

APawn* URGAbility::GetOwner() const
{
    return PawnOwner;
}

void URGAbility::SetOwner(APawn* NewOwner)
{
    PawnOwner = NewOwner;
}
