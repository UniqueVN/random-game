// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomGame.h"
#include "RGProjectile.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"


ARGProjectile::ARGProjectile(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    CollisionComp = ObjectInitializer.CreateDefaultSubobject<UCapsuleComponent>(this, TEXT("CollisionComp"));
    CollisionComp->SetCapsuleRadius(20.f);
    CollisionComp->SetCapsuleHalfHeight(20.f);
    RootComponent = CollisionComp;

    ProjectileMovement = ObjectInitializer.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileMovementComp"));
    ProjectileMovement->UpdatedComponent = CollisionComp;
    ProjectileMovement->InitialSpeed = 1000.f;
    ProjectileMovement->MaxSpeed = 1000.f;
    ProjectileMovement->bRotationFollowsVelocity = true;
    ProjectileMovement->bShouldBounce = false;
}


