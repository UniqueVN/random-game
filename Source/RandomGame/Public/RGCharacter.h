// Random Game Studios Copyright 2014 - 2014
// Base Character class for the RandomGame. Used by both AI and player
// 

#pragma once

#include "GameFramework/Character.h"
#include "RGAbility.h"
#include "RGCharacter.generated.h"

USTRUCT()
struct FRegisteredMove
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TSubclassOf<class URGMove> MoveType;

	UPROPERTY()
	class URGMove* MoveInstance;
};

/**
 * 
 */
UCLASS()
class RANDOMGAME_API ARGCharacter : public ACharacter
{
    GENERATED_BODY()

public:
    ARGCharacter(const class FObjectInitializer& ObjectInitializer);

    UFUNCTION(BlueprintCallable, Category = "Ability", FriendlyName = "AddAbility")
    URGAbility* AddAbility(TSubclassOf<class URGAbility> NewAbilityClass);

    UFUNCTION(BlueprintCallable, Category = "Ability", FriendlyName = "ActivateAbility")
    void ActivateAbility(URGAbility* Ability, APawn* PawnTarget = NULL);

    // Activate the ability at a specific index in the Abilities list
    UFUNCTION(BlueprintCallable, Category = "Ability", FriendlyName = "ActivateAbilityAtIndex", meta = (Latent, WorldContext = "WorldContextObject", LatentInfo = "LatentInfo"))
    void ActivateAbilityAtIndex(int32 Index, APawn* PawnTarget, FLatentActionInfo LatentInfo);

    UFUNCTION(BlueprintCallable, Category = "Move", FriendlyName = "PickMove")
	class URGMove* PickMove(TSubclassOf<URGMove> moveType);

	UFUNCTION(BlueprintImplementableEvent, Category = "Move", FriendlyName = "PickMovement")
	virtual class URGMove* K2_PickMovement(const FVector& Direction);

	bool MakeMove(float Duration);
	void OnMoveFinished();

    virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

    /** Event when this actor takes ANY damage */
    void ReceiveAnyDamage(float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

    UFUNCTION(BlueprintCallable, Category = "RGCharacter")
    void Heal(float HealAmount);

    UFUNCTION(BlueprintCallable, Category = "RGCharacter")
    bool IsDead();

    UFUNCTION(BlueprintNativeEvent, Category = "RGCharacter")
    void OnDead();

    UFUNCTION(BlueprintNativeEvent, Category = "RGCharacter")
    void SpawnPickup();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
    TArray<TSubclassOf<class URGAbility>>	AbilityClasses;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability")
    TArray<URGAbility*>	Abilities;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RGCharacter")
    float Health;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RGCharacter")
    float Energy;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

private:

	void HandleMoveInput(const FVector& Direction);

	UPROPERTY()
	TArray<FRegisteredMove> RegisteredMoves;
	
	UPROPERTY()
	class URGMove* PickedMove;

};
