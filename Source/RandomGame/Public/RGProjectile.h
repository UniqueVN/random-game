// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "RGProjectile.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API ARGProjectile : public AActor
{
	GENERATED_BODY()
public:
    ARGProjectile(const class FObjectInitializer& ObjectInitializer);

    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Projectile, meta = (AllowPrivateAccess = "true"))
    class UCapsuleComponent* CollisionComp;

    /** Projectile movement component */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Projectile, meta = (AllowPrivateAccess = "true"))
    class UProjectileMovementComponent* ProjectileMovement;
};
