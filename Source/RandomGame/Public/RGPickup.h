// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RGInteractable.h"
#include "RGPickup.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API ARGPickup : public ARGInteractable
{
	GENERATED_BODY()

public:
    ARGPickup(const class FObjectInitializer& ObjectInitializer);
};
