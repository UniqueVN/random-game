// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "RGCharacter.h"
#include "RGMove.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class RANDOMGAME_API URGMove : public UObject
{
	GENERATED_BODY()

public:
    URGMove(const class FObjectInitializer& ObjectInitializer);

	void Init(class ARGCharacter* Char);
	void Reset();

	void Execute(float Duration);
	
	UFUNCTION(BlueprintCallable, Category = "Move", FriendlyName = "FinishMove")
	void Finish();

	void Tick(float DeltaSeconds);

	bool IsRunning() { return Running; }

protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "Move", FriendlyName = "OnExecuted")
	virtual void K2_OnExecuted(class ARGCharacter* Owner, float Duration);

	UFUNCTION(BlueprintImplementableEvent, Category = "Move", FriendlyName = "OnTicked")
	virtual void K2_OnTicked(float DeltaSeconds);

private:

	UPROPERTY()
	class ARGCharacter* Owner;

	bool Running;
	
};
