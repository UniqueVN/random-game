// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "RandomGameGameMode.generated.h"

enum GameTurnState
{
	GameTurnStateWaiting,
	GameTurnStateProcessing,
};

UCLASS(minimalapi)
class ARandomGameGameMode : public AGameMode
{
	GENERATED_BODY()


public:
    ARandomGameGameMode(const class FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaSeconds) override;

	void OnPlayerMovePicked();
	void OnCharacterMoveFinished(class ARGCharacter* Char);

	GameTurnState GetTurnState() { return TurnState; }

private:

	void StartWaitingTurn();
	void StartProcessingTurn();

	UPROPERTY()
	float MoveDuration;

	GameTurnState TurnState;
	int NumPendingMoves;
};



