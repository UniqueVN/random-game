// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "RGAbility.generated.h"


//class UGameplayAbility;

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class RANDOMGAME_API URGAbility : public UObject
{
	GENERATED_BODY()

public:
    URGAbility(const class FObjectInitializer& ObjectInitializer);

    /** Returns true if this ability can be activated right now. Has no side effects */
    UFUNCTION(BlueprintCallable, Category = "Ability", FriendlyName = "CanBeTrigger")
    virtual bool CanBeTrigger(APawn* Target = NULL) const;

    UFUNCTION(BlueprintCallable, Category = "Ability", FriendlyName = "Activate")
    virtual void Activate(APawn* NewPawnTarget);

    UFUNCTION(BlueprintCallable, Category = "Ability", FriendlyName = "Deactivate")
    virtual void Deactivate();

    UFUNCTION(BlueprintCallable, Category = "Ability", FriendlyName = "GetOwner")
    APawn* GetOwner() const;

    void SetOwner(APawn* NewOwner);

protected:
    /** Pawn that owns this ability. */
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability")
    APawn* PawnOwner;
    
    /** Pawn that affected by this ability. */
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability")
    APawn* PawnTarget;

    UPROPERTY()
    bool bIsActivated;

    UFUNCTION(BlueprintImplementableEvent, Category = Ability, FriendlyName = "OnActivatedEvent")
    virtual void K2_OnActivatedEvent(class APawn* NewPawnTarget);

    UFUNCTION(BlueprintImplementableEvent, Category = Ability, FriendlyName = "OnDeactivatedEvent")
    virtual void K2_OnDeactivatedEvent();
};
