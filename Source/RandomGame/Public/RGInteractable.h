// Random Game Studios Copyright 2014 - 2014
// Base class for all the actor that player can interact (e.g: trigger, use, pick up ...) with
// 

#pragma once

#include "GameFramework/Actor.h"
#include "RGInteractable.generated.h"

/**
 * 
 */
UCLASS()
class RANDOMGAME_API ARGInteractable : public AActor
{
    GENERATED_BODY()

public:
    ARGInteractable(const class FObjectInitializer& ObjectInitializer);

    UFUNCTION(BlueprintCallable, Category = "Interactable", FriendlyName = "Activate")
    virtual void Activate(AActor* Activator);

    UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", FriendlyName = "Activate")
    virtual void OnActivated(AActor* Activator);

    UFUNCTION(BlueprintImplementableEvent, Category = "Interactable", FriendlyName = "Touch")
    virtual void OnTouch(AActor* Activator);

    /** Called when the collision capsule touches another primitive component */
    UFUNCTION()
    void OnCollisionCompTouch(AActor* Other, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

    UFUNCTION(BlueprintCallable, Category = "Interactable", FriendlyName = "ShouldHandleActorTouch")
    bool ShouldHandleActorTouch(AActor* Other) const;

    /** BoxComponent to specify the area where the actor detect other pawn start interacting. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Collision)
    class UBoxComponent* CollisionComp;

    /** The main static mesh associated with this interactable actor (optional sub-object). */
    UPROPERTY(Category = Visual, VisibleAnywhere, BlueprintReadOnly)
    class UStaticMeshComponent* Mesh;

    /**  List of classes of actors who can trigger this object */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable")
    TArray<TSubclassOf<class AActor>> TriggerableActorClasses;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
    bool bIsActive;

    /** If true then the actor will be activated right when it touched by other actor */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactable)
    bool bActivateInTouch;
};
