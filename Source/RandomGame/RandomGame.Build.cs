// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class RandomGame : ModuleRules
{
	public RandomGame(TargetInfo Target)
	{
        //PrivateIncludePaths.Add("Runtime/GameplayAbilities/Private");

        //PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "AIModule", "GameplayAbilities" });
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "AIModule"});
    }
}
